function show() {
  var point = document.getElementById("point").value * 1;
  var area = document.getElementById("area").value * 1;
  var target = document.getElementById("target").value * 1;
  var subject1 = document.getElementById("subject-1").value * 1;
  var subject2 = document.getElementById("subject-2").value * 1;
  var subject3 = document.getElementById("subject-3").value * 1;
  if (subject1 <= 0 || subject2 <= 0 || subject3 <= 0) {
    document.getElementById("result-1").innerText =
      "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
    return;
  }
  if (subject1 + subject2 + subject3 + area + target < point) {
    document.getElementById("result-1").innerText = `Bạn đã rớt. Tổng điểm: ${
      subject1 + subject2 + subject3 + area + target
    }`;
  } else {
    document.getElementById("result-1").innerText = `Bạn đã đậu. Tổng điểm: ${
      subject1 + subject2 + subject3 + area + target
    }`;
  }
}

function electricity() {
  var fullName = document.getElementById("full-name").value;
  var kw = document.getElementById("kw").value * 1;
  if (kw <= 0) {
    window.alert("Số kw không hợp lệ! Vui lòng nhập lại");
    document.getElementById(
      "result-2"
    ).innerText = `Họ tên: ${fullName}; Tiền điện: 0`;
    return;
  }
  var electricity = 0;
  if (kw <= 50) {
    electricity = kw * 500;
  } else if (kw <= 100) {
    electricity = 50 * 500 + (kw - 50) * 650;
  } else if (kw <= 200) {
    electricity = 50 * 500 + 50 * 650 + (kw - 100) * 850;
  } else if (kw <= 350) {
    electricity = 50 * 500 + 50 * 650 + 100 * 850 + (kw - 200) * 1100;
  } else {
    electricity =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kw - 350) * 1300;
  }
  document.getElementById(
    "result-2"
  ).innerText = `Họ tên: ${fullName}; Tiền điện: ${new Intl.NumberFormat().format(
    electricity
  )}`;
}

function tax() {
  var fullName = document.getElementById("full-name-1").value;
  var annualIncome = document.getElementById("annual-income").value * 1;
  var dependents = document.getElementById("dependents").value * 1;
  var taxableIncome = annualIncome - 4000000 - dependents * 1600000;
  if (taxableIncome <= 0) {
    window.alert("Số tiền thu nhập không hợp lệ");
    document.getElementById(
      "result-3"
    ).innerText = `Họ tên: ${fullName}; Tiền thuế thu nhập cá nhân: 0 VND`;
    return;
  }
  var taxRate = 0;
  if (taxableIncome <= 60e6) {
    taxRate = 5;
  } else if (taxableIncome <= 120e6) {
    taxRate = 10;
  } else if (taxableIncome <= 210e6) {
    taxRate = 15;
  } else if (taxableIncome <= 384e6) {
    taxRate = 20;
  } else if (taxableIncome <= 624e6) {
    taxRate = 25;
  } else if (taxableIncome <= 960e6) {
    taxRate = 30;
  } else {
    taxRate = 35;
  }
  document.getElementById(
    "result-3"
  ).innerText = `Họ tên: ${fullName}; Tiền thuế thu nhập cá nhân: ${new Intl.NumberFormat().format(
    (taxableIncome * taxRate) / 100
  )} VND`;
}

function cable() {
  var customer = document.getElementById("customer").value;
  var number = document.getElementById("customer-number").value;
  var channels = document.getElementById("premium-channels").value * 1;
  var connections = document.getElementById("connections").value * 1;

  if (customer == "0") {
    window.alert("Hãy chọn loại khách hàng");
    document.getElementById(
      "result-4"
    ).innerText = `Mã khách hàng: ${number}; Tiền cáp: $0.00`;
    return;
  }
  var total = 0.0;
  if (customer == "1") {
    total = 4.5 + 20.5 + channels * 7.5;
  } else {
    if (connections <= 10) {
      total = 15 + 75 + channels * 50;
    } else {
      total = 15 + 75 + (connections - 10) * 5 + channels * 50;
    }
  }
  document.getElementById(
    "result-4"
  ).innerText = `Mã khách hàng: ${number}; Tiền cáp: $${parseFloat(
    total
  ).toFixed(2)}`;
}

function switchConnection() {
  var customer = document.getElementById("customer").value;
  if (customer == "2") {
    document.getElementById("connections").classList.remove("d-none");
  } else {
    document.getElementById("connections").classList.add("d-none");
  }
}
